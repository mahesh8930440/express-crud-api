# Express HTTP CRUD API

## Introduction:

- This project implements a simple HTTP API for managing todos using the CRUD (Create, Read, Update, Delete) operations. Todos are stored in a PostgreSQL database, and the API is built using the Express.js framework.

## Prerequisites:

- Node.js
- PostgreSQL database
- Express.js

## Getting Started

## Steps to setup and run the project

1.  Clone this repository to your local machine:

    - git clone

2.  Install project dependencies:

    - cd `Express-HTTP-CRUD-API`
    - npm install
    - npm install sequelize pg

3.  Create a `.env ` File in the project's root directory

4.  Install dotenv Package:

    - npm install dotenv

5.  Start the application:

    - npm start
