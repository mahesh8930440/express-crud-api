const express = require("express");
const router = express.Router();

const { Sequelize } = require("sequelize");
const { DataTypes } = require("sequelize");
require("dotenv").config();
const {
  postUserSchema,
  getDeleteUserSchema,
  putUserSchema,
} = require("../userValidation");
const validation = require("../validationMiddleware");

const sequelize = new Sequelize(
  process.env.PG_NAME,
  process.env.PG_USER,
  process.env.PG_PASSWORD,
  {
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    dialect: "postgres",
    logging: true,
  }
);

const Todos = sequelize.define("Todos", {
  text: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  isCompleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  },
});

Todos.sync({ force: true });

router.post("/", validation(postUserSchema), async (request, response) => {
  try {
    const { text, isCompleted } = request.body;
    const todo = await Todos.create({ text, isCompleted });
    response.status(201).json(todo);
  } catch (err) {
    response.status(500).json({
      status: "error",
      message: err.message,
    });
  }
});

router.get(
  "/:id",
  validation(getDeleteUserSchema),
  async (request, response) => {
    try {
      const { id } = request.params;
      const todo = await Todos.findByPk(id);

      if (todo) {
        response.json(todo);
      } else {
        response.status(404).json({ message: "Todo not found" });
      }
    } catch (err) {
      response.status(500).json({
        status: "error",
        message: err.message,
      });
    }
  }
);

router.get("/", async (request, response) => {
  try {
    const todos = await Todos.findAll();
    response.json(todos);
  } catch (err) {
    response.status(500).json({
      status: "error",
      message: err.message,
    });
  }
});

router.put("/:id", validation(putUserSchema), async (request, response) => {
  try {
    const { id } = request.params;
    const { text } = request.body;

    const todo = await Todos.findByPk(id);
    if (todo) {
      await todo.update({ text });
      response.json(todo);
    } else {
      response.status(404).json({ message: "Todo not found" });
    }
  } catch (err) {
    response.status(500).json({
      status: "error",
      message: err.message,
    });
  }
});

router.delete("/:id", async (request, response) => {
  try {
    const { id } = request.params;

    const todo = await Todos.findByPk(id);
    if (todo) {
      await todo.destroy();
      response.json({ message: `Deleted the row with id ${id}` });
    } else {
      response.status(404).json({ message: "Todo not found" });
    }
  } catch (err) {
    response.status(500).json({
      status: "error",
      message: err.message,
    });
  }
});

module.exports = router;
