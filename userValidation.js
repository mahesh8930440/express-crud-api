const yup =require("yup");

const postUserSchema=yup.object().shape({
    text: yup.string().required(),
    isCompleted:yup.boolean().required()
});

const getDeleteUserSchema= yup.object().shape({
    id: yup.number().positive().integer().required()
})

const putUserSchema=yup.object().shape({
    id : yup.number().positive().integer().required(),
    text: yup.string().required()
});

module.exports={postUserSchema,getDeleteUserSchema,putUserSchema};